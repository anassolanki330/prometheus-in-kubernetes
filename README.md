### Steps to deploy promethues on Kuberbnetes

1) Create config map using following command:
 
 
```  kubectl create configmap prometheus-example-cm --from-file prometheus.yml ```

NOTE: use current prometheus conf file given in repo for configuration of prometheus 

2) Run deployment file using following command:

``` kubectl apply -f promDeploy.yml```

3) Create service using following command:

``` kubectl apply -f promService.yml ```


### To monitor a Kubernetes service

1) Install traefik 

``` kubectl create -f https://raw.githubusercontent.com/mateobur/prometheus-monitoring-guide/master/traefik-prom.yaml ```

Once the Traefik pods is running and you can display the service IP

2) Now, you need to add the new target to the prometheus.yml conf file:

Add below static point in prometheus.yml
```
  - job_name: 'traefik'
    static_configs:
    - targets: ['traefik:8080']
```

3) Patch the ConfigMap and Deployment:

``` 
kubectl create configmap prometheus-example-cm --from-file=prometheus.yml -o yaml --dry-run | kubectl apply -f -

kubectl patch deployment prometheus-deployment -p 
  "{"spec":{"template":{"metadata":{"labels":{"date":"`date +'%s'`"}}}}}"
```
